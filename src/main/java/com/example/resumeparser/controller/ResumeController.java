package com.example.resumeparser.controller;

import com.example.resumeparser.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@CrossOrigin
@RestController
@RequestMapping("/resume")
public class ResumeController {

    private ResumeService service;

    @Autowired
    public ResumeController(ResumeService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity checkResumeById(@PathVariable String id) throws IOException {
        boolean isWork = service.checkID(id);
        return isWork == true ? ResponseEntity.ok(service.getResumeById(id)) : ResponseEntity.ok(service.incorrectURL());
    }

    @GetMapping("/{id}/{format}")
    public ResponseEntity returnResumeById(@PathVariable String id, @PathVariable String format) {
        boolean withName = service.withName(format);
        return ResponseEntity.ok(service.getFullResume(id, withName));
    }
}
