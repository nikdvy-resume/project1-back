FROM maven:3.8.1-openjdk-11 as maven

COPY . /usr/src/myapp
WORKDIR /usr/src/myapp

RUN mvn package spring-boot:repackage


FROM openjdk:11.0.11-jre

ARG JAVA_PARAM="-Xms256M -Xmx1024M -XX:+UseG1GC"
ENV JAVA_PARAM=$JAVA_PARAM

WORKDIR /opt/app 

COPY --from=maven /usr/src/myapp/target/*.jar /opt/app/app.jar

RUN chown nobody -R /opt/app

USER 65534

ENV PATH=$PATH:/opt/app

EXPOSE 8080

CMD ["sh", "-c", "java ${JAVA_PARAM} -Dfile.encoding=UTF-8 -jar /opt/app/app.jar"]
# -Dserver.port=3007
